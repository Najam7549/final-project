package com.example.expense;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import androidx.annotation.Nullable;

public class DBHelper extends SQLiteOpenHelper {
    public DBHelper(Context context) {
        super(context, "AllBudget1.db", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create Table Budget(bname TEXT primary key)");
        db.execSQL("create Table Income(Id INTEGER primary key autoincrement ,Iname TEXT  , iamt INTEGER ,bname TEXT)");
        db.execSQL("create Table Expense(Id INTEGER primary key autoincrement,Ename TEXT,eamt INTEGER,bname TEXT)");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop Table if exists Budget");
        db.execSQL("drop Table if exists Income");
        db.execSQL("drop Table if exists Expense");
    }
    public Boolean insertuserdata(String bname){
        SQLiteDatabase DB=this.getWritableDatabase();
        ContentValues contentValues= new ContentValues();
        contentValues.put("bname",bname);


            long result=DB.insert("Budget",null,contentValues);
            if(result==-1){
                return false;
            }
            else{
                return true;
            }


    }
    public Boolean insertincomedata(String bname,String Iname,int iamt){
        SQLiteDatabase DB=this.getWritableDatabase();
        ContentValues contentValues= new ContentValues();
        contentValues.put("bname",bname);
        contentValues.put("Iname",Iname);
        contentValues.put("iamt",iamt);

        long result=DB.insert("Income",null,contentValues);
        if(result==-1){
            return false;
        }
        else{
            return true;
        }


    }
    public Boolean insertexpensedata(String bname,String ename,int eamt){
        SQLiteDatabase DB=this.getWritableDatabase();
        ContentValues contentValues= new ContentValues();
        contentValues.put("bname",bname);
        contentValues.put("Ename",ename);
        contentValues.put("eamt",eamt);

        long result=DB.insert("Expense",null,contentValues);
        if(result==-1){
            return false;
        }
        else{
            return true;
        }


    }

    public Boolean deleteincomedata(int Iid) {
        SQLiteDatabase DB = this.getWritableDatabase();
        Cursor cursor = DB.rawQuery("Select * from Income ", new String[]{});
        if (cursor.getCount() > 0) {
            long result = DB.delete("Income", "Id=?", new String[]{String.valueOf(Iid)});
            if (result == -1) {
                return false;
            } else {
                return true;
            }
        }
        else
        {
            return false;

        }
    }
    public Cursor getdata() {
        SQLiteDatabase DB = this.getWritableDatabase();
        Cursor cursor = DB.rawQuery("Select * from Budget", null);
        return  cursor;
    }
    public Cursor incomegetdata(String budgname) {
        SQLiteDatabase DB = this.getWritableDatabase();
        Cursor cursor = DB.rawQuery("Select * from Income where bname=?", new String[]{budgname});
        return  cursor;
    }
    public Cursor incomesum(String budgname) {
        SQLiteDatabase DB = this.getWritableDatabase();
        Cursor cursor = DB.rawQuery("Select Sum(iamt) from Income where bname=?", new String[]{budgname});
        return  cursor;
    }
    public Cursor expensesum(String budgname) {
        SQLiteDatabase DB = this.getWritableDatabase();
        Cursor cursor = DB.rawQuery("Select Sum(eamt) from Expense where bname=?", new String[]{budgname});
        return  cursor;
    }
    public Cursor expensegetdata(String budgname) {
        SQLiteDatabase DB = this.getWritableDatabase();
        Cursor cursor = DB.rawQuery("Select * from Expense where bname=?", new String[]{budgname});
        return  cursor;
    }

}
