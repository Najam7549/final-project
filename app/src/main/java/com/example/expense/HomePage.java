package com.example.expense;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
public class HomePage extends AppCompatActivity {
//    Button shIncome,shExpense,Addinc,Addexp;
//    EditText inctitle,incamount;
//    TextView textView13,bgname;
    LinearLayout explay;
    DBHelper DB;
    ArrayList<String> addArray= new ArrayList<String>();
    ListView show;

    public void chk(){
        String arr[]={};
        int n = arr.length;
        String newarr[]=new String[n+1];


        Cursor res=DB.getdata();

        if(res.getCount()==0){
            Toast.makeText(HomePage.this,"No Data Found",Toast.LENGTH_SHORT).show();

        }
        StringBuffer buffer=new StringBuffer();

        while (res.moveToNext()){



            if (addArray.contains(res.getString(0))){
                Toast.makeText(getBaseContext(),"Already added",Toast.LENGTH_SHORT).show();

            }
            else if(res.getString(0)==null){
                Toast.makeText(getBaseContext(),"Input field is empty",Toast.LENGTH_SHORT).show();

            }
            else{
                addArray.add(res.getString(0));
                ArrayAdapter<String> adapter=new ArrayAdapter<String>(HomePage.this, android.R.layout.simple_list_item_1,addArray);
                show.setAdapter(adapter);
                show.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        String selectedItem = (String) parent.getItemAtPosition(position);
//                        Toast.makeText(getBaseContext(),"Selected item is = "+selectedItem,Toast.LENGTH_SHORT).show();

                        Intent bgdetail = new Intent(HomePage.this,BudgetDetail.class);
                        bgdetail.putExtra("budgetname",selectedItem);
                        startActivity(bgdetail);

                    }
                });
                // show=(ListView)findViewById(R.id.listview1) ; show is list view which is declared above
                // res.getstring gets the data from database

                // listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                //         @Override
//                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                    String selectedItem = (String) parent.getItemAtPosition(position);
//                    textView.setText("The best football player is : " + selectedItem);
//                }
//            });

            }

        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.activity_mymenu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.new_budget:
                Intent newbg = new Intent(HomePage.this,MainActivity.class);

                startActivity(newbg);
                return true;
//            case R.id.allbudget:
//                chk();
//                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);
//        shIncome=(Button)findViewById(R.id.shinc);
//        shExpense=(Button)findViewById(R.id.shexp);
//        Addinc=(Button)findViewById(R.id.btn_add_income);
//        Addexp=(Button)findViewById(R.id.btn_exp_income);
//        inctitle=(EditText)findViewById(R.id.incometitle);
//        bgname=(TextView)findViewById(R.id.bname);
//        incamount=(EditText)findViewById(R.id.incomeamount) ;
//        explay=(LinearLayout)findViewById(R.id.explayout);
        DB= new DBHelper(this);
//        textView13=(TextView)findViewById(R.id.textView13);
        show=(ListView)findViewById(R.id.listview1) ;
        chk();



    }
}