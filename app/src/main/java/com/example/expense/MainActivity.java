package com.example.expense;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    Button BtnCreateBg,showdata,next;
    EditText bgname;
    DBHelper DB;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BtnCreateBg=(Button)findViewById(R.id.btn_create_bg);
        showdata=(Button)findViewById(R.id.showdata);
        next=(Button)findViewById(R.id.nexts);
        bgname=(EditText)findViewById(R.id.bgname);
        DB= new DBHelper(this);

        BtnCreateBg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String budget_name=bgname.getText().toString();
                Boolean check_insert_data=DB.insertuserdata(budget_name);
                if(budget_name==""){
                    Toast.makeText(MainActivity.this,"Please Enter Budget Name"+budget_name,Toast.LENGTH_SHORT).show();

                }
                else {
                    if(check_insert_data==false){
                        Toast.makeText(MainActivity.this,"Inserted Data exists or null ",Toast.LENGTH_SHORT).show();

                    }
                    else{
                        Toast.makeText(MainActivity.this,"Inserted budget name = "+budget_name,Toast.LENGTH_SHORT).show();
                    Intent i=new Intent(MainActivity.this,HomePage.class);
                    startActivity(i);
                    setContentView(R.layout.activity_home_page);
                    }
                }


            }
        });

        showdata.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Cursor res=DB.getdata();
                if(res.getCount()==0){
                    Toast.makeText(MainActivity.this,"No Data Found",Toast.LENGTH_SHORT).show();

                }
                    StringBuffer buffer=new StringBuffer();
                    while (res.moveToNext()){
                        buffer.append("Budget Name : "+res.getString(0)+"\n");
                    }
                    AlertDialog.Builder builder=new AlertDialog.Builder(MainActivity.this);
                    builder.setCancelable(true);
                    builder.setTitle("All Budgets");
                    builder.setMessage(buffer.toString());
                    builder.show();

            }
        });

        Cursor res=DB.getdata();
        if(res.getCount()>0) {
            next.setVisibility(View.VISIBLE);
        }


        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(MainActivity.this,HomePage.class);
                startActivity(i);
                setContentView(R.layout.activity_home_page);
            }
        });
    }
}