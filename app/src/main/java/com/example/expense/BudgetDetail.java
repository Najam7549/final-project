package com.example.expense;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.BoringLayout;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class BudgetDetail extends AppCompatActivity {

    Button shIncome,shExpense,Addinc,Addexp;
    EditText inctitle,incamount,expamount,exptitle;
    TextView textView13,bgname,tincome,texpense,tsaving;
    LinearLayout explay;
    DBHelper DB;
    ArrayList<String> addArray= new ArrayList<String>();
    ArrayList<String> addArrayexp= new ArrayList<String>();
    String budg;
    String incomename;
    ListView show,showexp;

    public void incdel(long in){
//        String s=Integer.toString(incn);
        int incn=(int) in;
        Boolean res=DB.deleteincomedata(incn);
        if(res==true){
            Toast.makeText(BudgetDetail.this,"Removed "+incn,Toast.LENGTH_SHORT).show();

        }
        else             {
            Toast.makeText(BudgetDetail.this,"Not Removed "+incn,Toast.LENGTH_SHORT).show();

        }

    }

    public void getincdata(){



        Cursor res=DB.incomegetdata(budg);

        if(res.getCount()==0){
            Toast.makeText(BudgetDetail.this,"No Data Found",Toast.LENGTH_SHORT).show();
            return;
        }

        while (res.moveToNext()){




                addArray.add(res.getString(1) +"\t\t\t\t\t\t\t\t\t\t\t\t"+res.getString(2));//android.R.layout.simple_list_item_1,addArray
                ArrayAdapter<String> adapter=new ArrayAdapter<String>(BudgetDetail.this, android.R.layout.simple_list_item_1,addArray);

                show.setAdapter(adapter);
                registerForContextMenu(show);
//          tincome
//                show.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                    @Override
//                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                        String selectedItem = (String) parent.getItemAtPosition(position);
//                        Toast.makeText(getBaseContext(),"Selected item is = "+selectedItem,Toast.LENGTH_SHORT).show();
//
////                            Intent bgdetail = new Intent(BudgetDetail.this,BudgetDetail.class);
////                            bgdetail.putExtra("budgetname",selectedItem);
////                            startActivity(bgdetail);
//
//                    }
//                });
                // show=(ListView)findViewById(R.id.listview1) ; show is list view which is declared above
                // res.getstring gets the data from database

                // listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                //         @Override
//                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                    String selectedItem = (String) parent.getItemAtPosition(position);
//                    textView.setText("The best football player is : " + selectedItem);
//                }
//            });




        }

    }
    public void getexpdata(){



        Cursor res=DB.expensegetdata(budg);

        if(res.getCount()==0){
            Toast.makeText(BudgetDetail.this,"No Data Found",Toast.LENGTH_SHORT).show();
            return;
        }

        while (res.moveToNext()){




                addArrayexp.add(res.getString(1) +"\t\t\t\t\t\t\t\t\t\t\t\t"+res.getString(2));//android.R.layout.simple_list_item_1,addArray
                ArrayAdapter<String> adapter=new ArrayAdapter<String>(BudgetDetail.this, android.R.layout.simple_list_item_1,addArrayexp);

                showexp.setAdapter(adapter);
                registerForContextMenu(show);

//                show.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                    @Override
//                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                        String selectedItem = (String) parent.getItemAtPosition(position);
//                        Toast.makeText(getBaseContext(),"Selected item is = "+selectedItem,Toast.LENGTH_SHORT).show();
//
////                            Intent bgdetail = new Intent(BudgetDetail.this,BudgetDetail.class);
////                            bgdetail.putExtra("budgetname",selectedItem);
////                            startActivity(bgdetail);
//
//                    }
//                });
                // show=(ListView)findViewById(R.id.listview1) ; show is list view which is declared above
                // res.getstring gets the data from database

                // listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                //         @Override
//                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                    String selectedItem = (String) parent.getItemAtPosition(position);
//                    textView.setText("The best football player is : " + selectedItem);
//                }
//            });



        }

    }



    @Override



    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.activity_mymenu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.new_budget:
                Intent newbg = new Intent(BudgetDetail.this,MainActivity.class);

                startActivity(newbg);
                return true;
            case R.id.allbudget:

                Intent bgdetail = new Intent(BudgetDetail.this,HomePage.class);

                startActivity(bgdetail);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.context_menu,menu);
        menu.setHeaderTitle("Select Action");
    }

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        switch (item.getItemId()) {
            case R.id.editt:

                Toast.makeText(getApplicationContext(),"Editing",Toast.LENGTH_SHORT).show();

                return true;
            case R.id.removee:
                incdel(info.id);
                return true;
            default:


            return super.onContextItemSelected(item);
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_budget_detail);
        bgname=(TextView)findViewById(R.id.bname1);
        Intent i=getIntent();
        budg=i.getStringExtra("budgetname");
        bgname.setText(budg);


        tincome=(TextView)findViewById(R.id.tincome);
        texpense=(TextView)findViewById(R.id.texpense);
        tsaving=(TextView)findViewById(R.id.tsaving);
        shIncome=(Button)findViewById(R.id.shinc1);
        shExpense=(Button)findViewById(R.id.shexp1);
        Addinc=(Button)findViewById(R.id.btn_add_income1);
        Addexp=(Button)findViewById(R.id.btn_exp_income1);
        inctitle=(EditText)findViewById(R.id.incometitle1);
        incamount=(EditText)findViewById(R.id.incomeamount1) ;
        exptitle=(EditText)findViewById(R.id.exptitle1);
        expamount=(EditText)findViewById(R.id.expamount1) ;
        explay=(LinearLayout)findViewById(R.id.explayout1);
        show=(ListView)findViewById(R.id.listview11);
        showexp=(ListView)findViewById(R.id.listview21);
        DB= new DBHelper(this);
//        textView13=(TextView)findViewById(R.id.textView13);




        shIncome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Addinc.getVisibility()  == View.VISIBLE) {
                    inctitle.setVisibility(View.INVISIBLE);
                    Addinc.setVisibility(View.INVISIBLE);
                    incamount.setVisibility(View.INVISIBLE);
                    show.setVisibility(View.INVISIBLE);
                    showexp.setVisibility(View.INVISIBLE);

                    Addexp.setVisibility(View.INVISIBLE);
                    explay.setVisibility(View.INVISIBLE);
                }
                else
                {
                    inctitle.setVisibility(View.VISIBLE);
                    Addinc.setVisibility(View.VISIBLE);
                    incamount.setVisibility(View.VISIBLE);
                    show.setVisibility(View.VISIBLE);
                    showexp.setVisibility(View.INVISIBLE);

                    Addexp.setVisibility(View.INVISIBLE);
                    explay.setVisibility(View.INVISIBLE);

                }
            }
        });
        shExpense.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Addexp.getVisibility()  == View.VISIBLE) {

                    Addexp.setVisibility(View.INVISIBLE);
                    explay.setVisibility(View.INVISIBLE);

                    inctitle.setVisibility(View.INVISIBLE);
                    Addinc.setVisibility(View.INVISIBLE);
                    incamount.setVisibility(View.INVISIBLE);
                    show.setVisibility(View.INVISIBLE);
                    showexp.setVisibility(View.INVISIBLE);

                }
                else
                {

                    explay.setVisibility(View.VISIBLE);
                    Addexp.setVisibility(View.VISIBLE);
                    showexp.setVisibility(View.VISIBLE);
                    inctitle.setVisibility(View.INVISIBLE);
                    Addinc.setVisibility(View.INVISIBLE);
                    incamount.setVisibility(View.INVISIBLE);
                    show.setVisibility(View.INVISIBLE);

                }
            }
        });
        Addinc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String ititle=inctitle.getText().toString();
                int iamt=Integer.parseInt (incamount.getText().toString());
                Boolean check_insert_data=DB.insertincomedata(budg,ititle,iamt);
                if(ititle==""){
                    Toast.makeText(BudgetDetail.this,"Please Enter Income Title",Toast.LENGTH_SHORT).show();

                }
                else {
                    if(check_insert_data==false){
                        Toast.makeText(BudgetDetail.this,"Not Inserted = "+budg+"=="+ititle +" = "+ iamt,Toast.LENGTH_SHORT).show();

                    }
                    else{
                        Toast.makeText(BudgetDetail.this,"Inserted ",Toast.LENGTH_LONG).show();
                        inctitle.setText(null);
                        incamount.setText(null);

                    }
                }

                finish();
                startActivity(getIntent());
            }
        });

        Addexp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String etitle=exptitle.getText().toString();
                int eamt=Integer.parseInt (expamount.getText().toString());
                Boolean check_insert_data=DB.insertexpensedata(budg,etitle,eamt);
                if(etitle==null){
                    Toast.makeText(BudgetDetail.this,"Please Enter Expense Title",Toast.LENGTH_SHORT).show();

                }
                else {
                    if(check_insert_data==false){
                        Toast.makeText(BudgetDetail.this,"Not Inserted = "+budg+"=="+etitle +" = "+ eamt,Toast.LENGTH_SHORT).show();

                    }
                    else{
                        Toast.makeText(BudgetDetail.this,"Inserted",Toast.LENGTH_LONG).show();
                        exptitle.setText(null);
                        expamount.setText(null);

                    }
                }
                finish();
                startActivity(getIntent());

            }
        });



        Cursor resum=DB.incomesum(budg);
        if(resum.moveToFirst()) {
//            Toast.makeText(BudgetDetail.this,"sum="+resum.getInt(0),Toast.LENGTH_LONG).show();
            tincome.setText("Total Income = "+resum.getInt(0));

        }
        Cursor exsum=DB.expensesum(budg);
        if(exsum.moveToFirst()) {
//            Toast.makeText(BudgetDetail.this,"EXP sum="+exsum.getInt(0),Toast.LENGTH_LONG).show();
            texpense.setText("Total Expense = "+exsum.getInt(0));

        }
        tsaving.setText("Savings "+ (resum.getInt(0)-exsum.getInt(0)));
        getincdata();
        getexpdata();
    }
}